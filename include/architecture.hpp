#include <sstream>
#include <mauve/tracing.hpp>
#include "laser.hpp"
#include "deros_observer.hpp"
#include "pltl_observer.hpp"

using namespace mauve::tracing;

struct ObserveLaserArchitecture : public LaserArchitecture {
  ObserveLaserArchitecture(double deros_freq, double pltl_freq, double laser_freq, double obs_laser_dur, double laser_max)
    : deros_freq(deros_freq), pltl_freq(pltl_freq), laser_freq(laser_freq)
    , obs_laser_duration(obs_laser_dur)
    , laser_max_value(laser_max)
  {};

  SharedData<LaserScan> & scan = mk_resource< SharedData<LaserScan> >("scan", LaserScan(1));
  DeRoSLaser & deros = mk_component<DeRoSLaser>("deros");
  PltlResource & pltl_res = mk_resource<PltlResource>("pltl_res");
  TimedObserver & pltl_observer = mk_component<TimedObserver>("pltl");
  Monitor & monitor = mk_component<Monitor>("monitor");

  template <typename T>
  void trace_config(const std::string& data, T& value) {
    std::stringstream ss;
    ss << value;
    trace_data(data, ss.str());
  };

  bool configure_hook() override {
    trace_config("laser_frequency", laser_freq);
    trace_config("deros_frequency", deros_freq);
    trace_config("pltl_frequency", pltl_freq);
    trace_config("range", laser_max_value);
    trace_config("duration", obs_laser_duration);

    laser.shell().scan.connect(scan.interface().write);
    laser.fsm().period = ms_to_ns(1000./laser_freq);

    laser.core().max_range = laser_max_value;
    laser.core().wcet = (1.0/laser_freq);

    deros.shell().scan.connect(scan.interface().read_value);
    deros.fsm().period = ms_to_ns(1000.0/deros_freq);
    deros.core().duration = sec_to_ns(obs_laser_duration);

    laser.shell().scan.connect(pltl_res.interface().write);

    pltl_res.core().duration = sec_to_ns(obs_laser_duration);

    pltl_observer.shell().read_formula.connect(pltl_res.interface().read_status);

    pltl_observer.core().duration = sec_to_ns(obs_laser_duration);
    pltl_observer.fsm().period = ms_to_ns(1000./pltl_freq);

    monitor.shell().read.connect(pltl_res.interface().read_all);
    monitor.fsm().period = ms_to_ns(1000./pltl_freq);

    laser.set_cpu(0);
    deros.set_cpu(1);
    pltl_observer.set_cpu(2);
    monitor.set_cpu(3);

    laser.set_priority(10);
    deros.set_priority(1);
    pltl_observer.set_priority(1);
    monitor.set_priority(1);

    return pltl_observer.configure()
     && pltl_res.configure()
     && deros.configure()
     && scan.configure()
     && monitor.configure()
     && LaserArchitecture::configure_hook();
  }

private:
  double deros_freq, pltl_freq, laser_freq;
  double obs_laser_duration;
  double laser_max_value;
};
