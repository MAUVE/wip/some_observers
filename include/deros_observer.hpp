#ifndef OBSERVER_DEROS
#define OBSERVER_DEROS

#include "laser.hpp"
#include "observer.hpp"

struct DeRosLaserShell : public Shell {
  ReadPort< LaserScan > & scan = mk_read_port< LaserScan >("scan", LaserScan(1));
};

struct DeRosLaserCore : public Core<DeRosLaserShell>, public Observer {
  Property<time_ns_t> & duration = mk_property<time_ns_t>("duration", sec_to_ns(2));

  LaserScan scan;

  bool configure_hook() override {
    logger().info("DeRoS observer duration: {}", duration.get());
    return Observer::configure(pltl_atom("scan<1", [&](){ return scan < 1; }), duration.get());
  };

  void read() {
    scan = shell().scan.read();
    update();
    logger().debug("all: {}", all->eval());
  };

};

struct DeRosLaserFSM : public FiniteStateMachine<DeRosLaserShell, DeRosLaserCore> {
  using C = DeRosLaserCore;
  Property<time_ns_t> & period = mk_property("period", ms_to_ns(10));
  ExecState<C> & read = mk_execution("Read", &C::read);
  ExecState<C> & failure = mk_execution("Failure", [&](C* c){ c->logger().info("Failure"); });
  ExecState<C> & read_failure = mk_execution("ReadFailure", &C::read);
  SynchroState<C> & sync_read = mk_synchronization("SyncRead", ms_to_ns(10));
  SynchroState<C> & sync_fail = mk_synchronization("SyncFail", ms_to_ns(10));
  bool configure_hook() override {
    logger().info("DeRoS period: {}", period.get());
    sync_read.set_clock(period.get());
    sync_fail.set_clock(period.get());

    set_initial(read);

    mk_transition(read, [&](C* c){ return c->all->eval(); }, failure);
    set_next(read, sync_read);
    set_next(sync_read, read);

    set_next(failure, sync_fail);

    mk_transition(read_failure, [&](C* c){ return ! c->all->eval(); }, sync_read);
    set_next(read_failure, sync_fail);
    set_next(sync_fail, read_failure);

    return true;
  };
};

using DeRoSLaser = Component<DeRosLaserShell, DeRosLaserCore, DeRosLaserFSM>;

#endif
