#ifndef OBSERVER_LASER
#define OBSERVER_LASER

#include <random>

#include <mauve/runtime.hpp>
#include <mauve/pastlogic.hpp>

#include <benchmarking/cpu.hpp>

using namespace mauve::runtime;

using LaserScan = double;

struct LaserShell : public Shell {
  WritePort<LaserScan> & scan = mk_write_port<LaserScan>("scan");
  ReadPort<unsigned int> & failure_mode = mk_read_port<unsigned int>("mode", 0);
};

struct LaserCore : public Core<LaserShell> {
  Property<double> & max_range = mk_property<double>("max_range", 3.0);
  Property<double> & wcet = mk_property<double>("wcet", 0.0);

  std::default_random_engine re;
  std::uniform_real_distribution<double> dis_range;
  std::uniform_real_distribution<double> dis_wcet;

  unsigned int mode;

  bool configure_hook() override {
    if (max_range.get() > 0) {
      decltype(dis_range.param()) new_range (0.0, max_range.get());
      dis_range.param(new_range);
      logger().info("max range: {}", max_range.get());
    }
    if (wcet.get() > 0) {
      decltype(dis_wcet.param()) new_wcet (0.0, wcet.get());
      dis_wcet.param(new_wcet);
      logger().info("wcet: {}", wcet.get());
    }
    return true;
  }

  void read_mode() {
    mode = shell().failure_mode.read();
    if (wcet.get() > 0)
      benchmarking::burn_cpu(dis_wcet(re)*1000*1000);
  };

  void write_scan() {
    LaserScan s = (max_range.get() > 0) ? dis_range(re) : 10;
    shell().scan.write(s);
  };

  void write_failure() {
    logger().info("Failure");
    shell().scan.write(0);
  };
};

struct LaserFSM : public FiniteStateMachine<LaserShell, LaserCore> {
  using C = LaserCore;
  Property<time_ns_t> & period = mk_property<time_ns_t>("period", ms_to_ns(10));
  ExecState<C> & read_mode = mk_execution("ReadMode", &C::read_mode);
  ExecState<C> & write_scan = mk_execution("WriteScan", &C::write_scan);
  ExecState<C> & failure = mk_execution("Failure", &C::write_failure);
  SynchroState<C> & sync = mk_synchronization("Sync", ms_to_ns(10));
  bool configure_hook() override {

    logger().info("Laser period: {}", period.get());
    sync.set_clock(period.get());

    set_initial(read_mode);
    set_next(sync, read_mode);
    mk_transition(read_mode, [&](C* c){ return c->mode > 0; }, failure);
    set_next(read_mode, write_scan);
    set_next(write_scan, sync);
    set_next(failure, sync);
    return true;
  };
};

using Laser = Component<LaserShell, LaserCore, LaserFSM>;

#include <mauve/ros.hpp>
#include <std_msgs/UInt32.h>
using namespace mauve::ros;

struct LaserArchitecture : Architecture {
  Laser & laser = mk_component<Laser>("laser");
  using Sub = SubscriberResource<std_msgs::UInt32, unsigned int>;
  Sub & sub = mk_resource<Sub>("subscriber", "scan_failure", conversions::convert<std_msgs::UInt32, unsigned int>);
  bool configure_hook() override {
    laser.shell().failure_mode.connect(sub.interface().read_value);
    sub.configure();
    laser.configure();
    return true;
  };
};

#endif
