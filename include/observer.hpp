#ifndef OBSERVER_H
#define OBSERVER_H

#include "laser.hpp"

using namespace mauve::past_logic;

struct ObserverStatus {
  bool v_f;
  time_ns_t t_f;
};

struct Observer : virtual WithLogger {
  ObserverStatus status { false, 0 };

  inline static time_ns_t now() {
    timeval tim;
    gettimeofday(&tim,NULL);
    return timeval_to_ns(tim);
  };

  bool configure(PastLTL* p, time_ns_t d) {
    this->atomic = p->clone();
    this->within = d;
    logger().info("Atomic: {}", atomic->to_string());
    all = pltl_and({ atomic,
      pltl_atom("(t-t_f)>=d", [&](){ return (now() - status.t_f) >= this->within; })});
    logger().info("All formula: {}", all->to_string());
    one = pltl_or({ atomic,
      pltl_atom("(t-t_f)<=d", [&](){ return (now() - status.t_f) <= this->within; })});
    logger().info("One formula: {}", one->to_string());
    return true;
  };

  PastLTL* atomic;
  PastLTL* all;
  PastLTL* one;
  time_ns_t within;

  void update() {
    auto x = atomic->eval();
    if (x != status.v_f) {
      status.v_f = x;
      status.t_f = now();
      logger().debug("status: {} {}", status.v_f, status.t_f);
    }
    all->step();
    one->step();
  };

};

#endif
