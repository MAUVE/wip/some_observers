#ifndef OBSERVER_PLTL
#define OBSERVER_PLTL

#include "laser.hpp"
#include "observer.hpp"

using namespace mauve::past_logic;

// Ressource
struct PltlResourceCore : public Core<Shell>, public Observer {
  Property<time_ns_t> & duration = mk_property<time_ns_t>("duration", sec_to_ns(2));
  LaserScan scan = 1;
  bool configure_hook() {
    logger().info("observer duration: {}", duration.get());
    return Observer::configure(pltl_atom("scan<1", [&](){ return scan < 1; }), duration.get());
  };

  void write(LaserScan scan) {
    this->scan = scan;
    update();
  };
};

struct PltlResourceInterface : public Interface<Shell, PltlResourceCore> {
  WriteService<LaserScan> & write = mk_write_service<LaserScan>("write", &PltlResourceCore::write);
  ReadService<ObserverStatus> & read_status = mk_read_service<ObserverStatus>("read_status",
    [&](PltlResourceCore* core) { return core->status; });
  ReadService<bool> & read_all = mk_read_service<bool>("read_all",
    [&](PltlResourceCore* core) { return core->all->eval(); });
  ReadService<bool> & read_one = mk_read_service<bool>("read_one",
    [&](PltlResourceCore* core) { return core->one->eval(); });
};

using PltlResource = Resource<Shell, PltlResourceCore, PltlResourceInterface>;

// Component
struct TimedObserverShell : public Shell {
  ReadPort<ObserverStatus> & read_formula = mk_read_port<ObserverStatus>("formula", ObserverStatus());
};
struct TimedObserverCore : public Core<TimedObserverShell> {
  Property<time_ns_t> & duration = mk_property<time_ns_t>("duration", sec_to_ns(2));

  ObserverStatus p;
  PastLTL* formula;

  bool configure_hook() override {
    formula = pltl_and({
      pltl_atom("v_t", [&](){ return p.v_f; }),
      pltl_atom("(t-t_f)>=d", [&](){ return (Observer::now() - p.t_f) >= duration.get(); })});
    logger().info("Observer: {}", formula->to_string());
    return true;
  };

  void read() {
    p = shell().read_formula.read();
    formula->step();
    logger().debug("p:{},{} phi:{} at {}", p.v_f, p.t_f, formula->eval(), Observer::now());
  };
};

struct TimedObserverFSM : public FiniteStateMachine<TimedObserverShell, TimedObserverCore> {
  using C = TimedObserverCore;
  Property<time_ns_t> & period = mk_property("period", ms_to_ns(10));
  ExecState<C> & read = mk_execution("Update", &C::read);
  ExecState<C> & failure = mk_execution("Failure", &C::read);
  SynchroState<C> & sync_read = mk_synchronization("SyncR", ms_to_ns(10));
  SynchroState<C> & sync_failure = mk_synchronization("SyncF", ms_to_ns(10));

  bool configure_hook() override {
    sync_read.set_clock(period.get());
    sync_failure.set_clock(period.get());

    set_initial(read);

    mk_transition(read, [&](C* c){ return c->formula->eval(); }, failure);
    set_next(read, sync_read);
    set_next(sync_read, read);

    mk_transition(failure, [&](C* c){ return ! c->formula->eval(); }, sync_read);
    set_next(failure, sync_failure);
    set_next(sync_failure, failure);
    return true;
  };
};

using TimedObserver = Component<TimedObserverShell, TimedObserverCore, TimedObserverFSM>;

// Component
struct MonitorShell : public Shell {
  ReadPort<bool> & read = mk_read_port<bool>("status", false);
};
struct MonitorCore : public Core<MonitorShell> {
  bool status;
  void read() {
    status = shell().read.read();
    logger().debug("p:{} at {}", status, Observer::now());
  };
};

struct MonitorFSM : public FiniteStateMachine<MonitorShell, MonitorCore> {
  using C = MonitorCore;
  Property<time_ns_t> & period = mk_property("period", ms_to_ns(10));
  ExecState<C> & read = mk_execution("Update", &C::read);
  ExecState<C> & failure = mk_execution("Failure", &C::read);
  SynchroState<C> & sync_read = mk_synchronization("SyncR", ms_to_ns(10));
  SynchroState<C> & sync_failure = mk_synchronization("SyncF", ms_to_ns(10));

  bool configure_hook() override {
    sync_read.set_clock(period.get());
    sync_failure.set_clock(period.get());

    set_initial(read);

    mk_transition(read, [&](C* c){ return c->status; }, failure);
    set_next(read, sync_read);
    set_next(sync_read, read);

    mk_transition(failure, [&](C* c){ return ! c->status; }, sync_read);
    set_next(failure, sync_failure);
    set_next(sync_failure, failure);
    return true;
  };
};

using Monitor = Component<MonitorShell, MonitorCore, MonitorFSM>;


#endif
