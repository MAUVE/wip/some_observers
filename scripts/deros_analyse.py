#! /usr/bin/env python3
from subprocess import *
from time import sleep
import babeltrace
from datetime import datetime
import random

def analyse_traces(trace_file,by="deros_frequency",verbose=True):
    trace_collection = babeltrace.TraceCollection()
    trace_collection.add_traces_recursive(trace_file, 'ctf')

    first_laser_failure = -1
    deros_failures = []
    pltl_failures = []
    monitor_failures = []

    previous_deros_failure = False
    previous_pltl_failure = False
    previous_monitor_failure = False

    for event in trace_collection.events:

        if "data" in event.name:
            if event["name"] == "laser_frequency": laser_frequency = int(event["value"])
            if event["name"] == "deros_frequency": deros_frequency = int(event["value"])
            if event["name"] == "laser_proba" or event["name"] == "range": fproba = float(event["value"])
            if event["name"] == "duration": duration = int(event["value"])

        if "state_execution_begin" in event.name:
            if "laser" in event["component"] and "Failure" in event["state"]:
                if first_laser_failure < 0:
                    first_laser_failure = event.timestamp
            if "deros" in event["component"]:
                if "Failure" in event["state"] and previous_deros_failure:
                    pass#print(event.timestamp, event["component"])
                elif "Read" in event["state"]:
                    previous_deros_failure = False
                elif "Failure" in event["state"] or "SyncFailing" in event["state"]:
                    previous_deros_failure = True
                    deros_failures += [event.timestamp]
            if "pltl" in event["component"]:
                if "Failure" in event["state"] and previous_pltl_failure:
                    pass#print(event.timestamp, event["component"])
                elif "Update" in event["state"]:
                    previous_pltl_failure = False
                elif "Failure" in event["state"]:
                    previous_pltl_failure = True
                    pltl_failures += [event.timestamp]
            if "monitor" in event["component"]:
                if "Failure" in event["state"] and previous_monitor_failure:
                    pass#print(event.timestamp, event["component"])
                elif "Update" in event["state"]:
                    previous_monitor_failure = False
                elif "Failure" in event["state"]:
                    previous_monitor_failure = True
                    monitor_failures += [event.timestamp]

    if verbose: print("Laser failure: {}".format(first_laser_failure))

    def stats(failures, t, dur):
        if len(failures) == 0:
            return 0, -1, 0

        false_detections = 0
        for f in failures:
            if f < t: false_detections += 1
            else: break
        delay = (f - t)*1e-9
        if delay < dur:
            return false_detections, delay + 1.0/laser_frequency, 1.0/laser_frequency
        else:
            return false_detections, delay, 0

    mc, mf, delta = stats(monitor_failures, first_laser_failure, duration)
    dc, df, _ = stats(deros_failures, first_laser_failure+delta, duration)
    print(mc, mf, delta, first_laser_failure)
    return ( fproba, duration, (laser_frequency if by=='laser_frequency' else deros_frequency), dc, df, 0, 0, mc, mf )

###
if __name__ == "__main__":
    print("# Session, FailureTime, LaserRange, FormulaDuration, DeRosFreq, DeRoSFalse, DeRoSDelay, PLTLFalse, PLTLDelay")
    now = datetime.now()
    out = open("deros"+str(now.isoformat())+".dat", 'w', 1)
    out.write("# Session, FailureTime, LaserRange, FormulaDuration, DeRosFreq, DeRoSFalse, DeRoSDelay, PLTLFalse, PLTLDelay\n")

    fail_time = 0

    to_remove = []

    import sys
    if len(sys.argv) > 0:
        sessions = sys.argv[1:]
    else:
        from pathlib import Path
        sessions = [pth.name for pth in Path.cwd().iterdir() if "laser_obs2018" in pth.name]

    analyse_by="laser_frequency"
    analyse_by="deros_frequency"

    for session in sessions:
        print(session)
        try:
            r, d, f, dc, df, pc, pf, mc, mf = analyse_traces(session,by=analyse_by,verbose=True)
            print("{},{},{},{},{},{},{},{},{},{},{}".format(session,fail_time,r,d,f,dc,df,pc,pf,mc,mf))
            if f >= 10 and (df < d):
                to_remove.append(session)
            out.write("{},{},{},{},{},{},{},{},{},{},{}\n".format(session,fail_time,r,d,f,dc,df,pc,pf,mc,mf))
        except Exception as ex:
            print(ex)
            continue

    out.close()
    print(to_remove)
    print("deros"+str(now.isoformat())+".dat")
