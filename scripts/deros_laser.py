#! /usr/bin/env python3
from subprocess import *
from time import sleep
import babeltrace
from datetime import datetime
import random
from deros_analyse import analyse_traces

def run(range, freqs, duration, debug=False):
    now = datetime.now()
    session = 'laser_obs' + now.isoformat()
    roscore = Popen('roscore', stdout=PIPE, stderr=PIPE)
    sleep(2)
    call(['lttng', 'create', '-o', session], stdout=PIPE, stderr=PIPE)
    call(['lttng', 'enable-event', '-u', 'mauve:*'], stdout=PIPE, stderr=PIPE)
    call(['lttng', 'start'], stdout=PIPE, stderr=PIPE)
    if debug:
        mauve = Popen(['rosrun', 'some_observers', 'laser_architecture', '-l', '/home/lesire/Work/mauve_ws/src/some_observers/mauve_logger.cfg',
                       '-r', str(range), '-f', str(freqs['laser']), '-o', str(freqs['deros']), '-p', str(freqs['pltl']), '-d', str(duration)])
    else:
        mauve = Popen(['rosrun', 'some_observers', 'laser_architecture',
            '-r', str(range), '-f', str(freqs['laser']), '-o', str(freqs['deros']), '-p', str(freqs['pltl']), '-d', str(duration)], stdout=PIPE, stderr=PIPE)
    fail_time = random.randint(5,15)
    sleep(fail_time)
    call(['rostopic', 'pub', '/scan_failure', 'std_msgs/UInt32', '1', '-1'], stdout=PIPE, stderr=PIPE)
    sleep(5)
    mauve.terminate()
    mauve.wait()
    call(['lttng', 'stop'], stdout=PIPE, stderr=PIPE)
    call(['lttng', 'destroy'], stdout=PIPE, stderr=PIPE)
    roscore.terminate()
    roscore.wait()
    return session, fail_time

if __name__ == '__main__':
    for r, d, f in [(10,1,{'pltl': 30, 'deros': 30, 'laser': f}) for f in [10,20,30,40,50] for _ in range(10)]:
        try:
            session, fail_time = run(range=r, freqs=f, duration=d, debug=False)
            print(r, d, f, session, fail_time)
        except Exception as ex:
            print(ex)
            continue
