#! /usr/bin/env python3

import pylab
import numpy as np

def parse_data(data):
    freqs = set([data[x][3] for x in range(len(data))])
    byfreq_deros_delay = {key: [] for key in freqs}
    byfreq_deros_false = {key: [] for key in freqs}
    byfreq_pltl_false = {key: [] for key in freqs}
    byfreq_pltl_delay = {key: [] for key in freqs}
    byfreq_mon_false = {key: [] for key in freqs}
    byfreq_mon_delay = {key: [] for key in freqs}


    def stat(row, f_index, d_index, falses, delays):
        if row[d_index] == -1: return
        f = row[f_index]
        d = row[d_index]-row[2]
        if d < 0:
            falses[row[3]].append(f+1)
        else:
            delays[row[3]].append(d)
            falses[row[3]].append(f)


    for row in data:
        stat(row, 4, 5, byfreq_deros_false, byfreq_deros_delay)
        stat(row, 6, 7, byfreq_pltl_false, byfreq_pltl_delay)
        stat(row, 8, 9, byfreq_mon_false, byfreq_mon_delay)

    return sorted(list(freqs)), byfreq_deros_delay, byfreq_deros_false, byfreq_pltl_delay, byfreq_pltl_false, byfreq_mon_delay, byfreq_mon_false

def plot_delays(ranges, byfreq_deros_delay, byfreq_pltl_delay, byfreq_mon_delay, name):
    tab = byfreq_deros_delay
    means = [np.sum(tab[x])/len(tab[x]) for x in ranges]
    #mins = [np.sum(tab[x])/len(tab[x])-np.std(tab[x]) for x in ranges]
    #maxs = [np.sum(tab[x])/len(tab[x])+np.std(tab[x]) for x in ranges]
    mins = [np.min(tab[x]) for x in ranges]
    maxs = [np.max(tab[x]) for x in ranges]
    pylab.plot((ranges), means, 'r^-', label='DeRoS')
    pylab.fill_between((ranges), mins, maxs, alpha=.2, facecolor='r')

    tab = byfreq_mon_delay
    means = [np.sum(tab[x])/len(tab[x]) for x in ranges]
    #mins = [np.sum(tab[x])/len(tab[x])-np.std(tab[x]) for x in ranges]
    #maxs = [np.sum(tab[x])/len(tab[x])+np.std(tab[x]) for x in ranges]
    mins = [np.min(tab[x]) for x in ranges]
    maxs = [np.max(tab[x]) for x in ranges]
    pylab.plot((ranges), means, 'gs-', label='PLTL-Monitor')
    pylab.fill_between((ranges), mins, maxs, alpha=.2, facecolor='g')

    flat_tab = [i for x in ranges for i in tab[x]]
    mean = np.sum(flat_tab)/len(flat_tab)
    std = np.std(flat_tab)
    pylab.plot(ranges, [mean]*len(ranges), 'b--')
    pylab.fill_between(ranges, [mean-std]*len(ranges), [mean+std]*len(ranges), alpha=.1, facecolor='b')

    pylab.xlabel('DeRoS observer frequency (Hz)')
    pylab.ylabel('detection delay (s)')
    pylab.xlim(0, max(ranges)+1)
    #pylab.plot([0]*50, 'black')
    #pylab.plot([1.1]*50, 'black')
    pylab.legend(loc="upper right")
    pylab.savefig(name+'.png')

def plot_false(ranges, deros, pltl, monitor, name):
    means = [np.sum(deros[x])/len(deros[x]) for x in ranges]
    maxs = [np.max(deros[x]) - (np.sum(deros[x])/len(deros[x])) for x in ranges]
    pylab.bar(ranges, means, yerr=[[0]*len(means),maxs], width=-.25, align='edge', label='DeRoS', color='red', ecolor='red')
    #means = [np.sum(pltl[x])/len(pltl[x]) for x in ranges]
    #maxs = [np.max(pltl[x]) - (np.sum(pltl[x])/len(pltl[x])) for x in ranges]
    #pylab.bar(ranges, means, yerr=[[0]*len(means),maxs], width=.25, align='edge', label='PLTL', color='blue', ecolor='blue')
    means = [np.sum(monitor[x])/len(monitor[x]) for x in ranges]
    maxs = [np.max(monitor[x]) - (np.sum(monitor[x])/len(monitor[x])) for x in ranges]
    pylab.bar(ranges, means, yerr=[[0]*len(means),maxs], width=.25, align='edge', label='PLTL-Monitor', color='green', ecolor='green')
    pylab.xlabel('DeRoS observer frequency (Hz)')
    pylab.ylabel('Number of False Detections')
    pylab.xlim(0, max(ranges)+1)
    pylab.legend(loc="upper right")
    pylab.savefig(name+'.png')

if __name__ == '__main__':
    import sys
    try:
        data = pylab.loadtxt(sys.argv[1], delimiter=',',usecols=range(1,11))
    except:
        data = pylab.loadtxt('deros.dat', delimiter=',',usecols=range(1,11))

    if len(data) > 0 and ( isinstance(data[0], int) or isinstance(data[0], float) ):
        data = [data]
    print(data)
    #for r in [2,3,5,10]:# for d in range(1,5)]:
    #    pylab.clf()
    #    ranges, deros_delay, deros_false, pltl_delay = parse_data(data, lambda x: x == r, lambda x: True)
    #    plot_delays(ranges, deros_delay, pltl_delay, 'figR='+str(r))

    pylab.clf()
    ranges, deros_delay, deros_false, pltl_delay, pltl_false, mon_delay, mon_false = parse_data(data)
    print(ranges)
    try:
        name = sys.argv[2]
    except:
        name = "deros"

    plot_delays(ranges, deros_delay, pltl_delay, mon_delay, name+'-delay')
    pylab.clf()
    plot_false(ranges, deros_false, pltl_false, mon_false, name+'-false')
