#include <fstream>
#include <random>

#include "architecture.hpp"
#include "popl.hpp"

int main(int argc, char** argv) {

  using namespace popl;

  std::random_device rd;
  std::mt19937 mt(rd());
  std::uniform_int_distribution<int> dist(1000, 2000);

  double deros_freq, laser_freq, pltl_freq, deros_dur, laser_max;

  OptionParser op("Allowed options");
  auto help_option          = op.add<Switch>("h", "help", "produce help message");
  auto deros_freq_option    = op.add<Value<double>>("o", "deros-freq", "DeRoS Laser frequency", 30, &deros_freq);
  auto laser_freq_option    = op.add<Value<double>>("f", "freq", "Laser frequency", 30, &laser_freq);
  auto pltl_freq_option     = op.add<Value<double>>("p", "pltl-freq", "PLTL Laser frequency", 30, &pltl_freq);
  auto deros_dur_option     = op.add<Value<double>>("d", "duration", "Observer Laser duration", 2, &deros_dur);
  auto mauve_logger_config  = op.add<Value<std::string>>("l", "logger", "MAUVE Logger config file");
  auto laser_max_option     = op.add<Value<double>>("r", "range", "Laser max range", 5.0, &laser_max);

  op.parse(argc, argv);

  if (help_option->is_set() == 1) {
		std::cout << op << std::endl;
    exit(0);
  }

  if (mauve_logger_config->is_set()) {
    std::ifstream config(mauve_logger_config->value());
    AbstractLogger::initialize(config);
  }

  ObserveLaserArchitecture archi(deros_freq, pltl_freq, laser_freq, deros_dur, laser_max);
  AbstractDeployer* depl = mk_deployer(&archi);
  assert(archi.configure());
  depl->create_tasks();
  depl->activate();
  /*
  auto now = depl->now();
  depl->start("laser", now + sec_to_ns(1));
  auto i = dist(mt);
  depl->start("deros", now + ms_to_ns(i));
  depl->start("pltl", now + ms_to_ns(i));
  */
  depl->start();
  depl->loop();

  depl->stop();
  depl->clear_tasks();
  archi.cleanup();

  return 0;
}
