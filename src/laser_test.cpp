#include "laser.hpp"

int main() {
  std::stringstream config;
  config << "default:" << std::endl;
  config << "  type: stdout" << std::endl;
  config << "  level: info" << std::endl;
  config << "laser:" << std::endl;
  config << "  - type: stdout" << std::endl;
  config << "    level: trace" << std::endl;
  AbstractLogger::initialize(config);

  LaserArchitecture archi;
  AbstractDeployer* depl = mk_deployer(&archi);
  archi.configure();
  depl->create_tasks();
  depl->activate();
  depl->start();

  depl->loop();

  depl->stop();
  depl->clear_tasks();
  archi.cleanup();

  return 0;
}
